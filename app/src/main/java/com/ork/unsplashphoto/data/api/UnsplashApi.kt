package com.ork.unsplashphoto.data.api

import com.ork.unsplashphoto.BuildConfig
import com.ork.unsplashphoto.data.model.UnsplashPhoto
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.Query

interface UnsplashApi {

    companion object {
        const val CLIENT_ID = BuildConfig.UNSPLASH_ACCESS_KEY
    }

    @Headers("Accept-Version: v1", "Authorization: Client-ID $CLIENT_ID")
    @GET("/photos")
    suspend fun getDataApi(@Query("page") query: Int,
   ): Response<List<UnsplashPhoto>>



}