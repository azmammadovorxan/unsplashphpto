package com.ork.unsplashphoto.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize


@Parcelize
data class  UnsplashPhoto(
    @SerializedName("description")
    val description:String?,
    @SerializedName("urls")
    val urls: Urls,
    @SerializedName("user")
    val user: User,
) : Parcelable {




}