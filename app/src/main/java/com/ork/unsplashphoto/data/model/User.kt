package com.ork.unsplashphoto.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class User(
    @SerializedName("name")
    val name:String?,
    @SerializedName("username")
    val username: String?,
    @SerializedName("twitter_username")
    val twitter_username:String?,
    @SerializedName("instagram_username")
    val instagram_username:String?,
    @SerializedName("portfolio_url")
    val portfolio_url:String?,
    @SerializedName("bio")
    val bio: String?,
    @SerializedName("profile_image")
    val profileImage:ProfileImage,
    ) : Parcelable{
    @Parcelize
    data class ProfileImage(
        @SerializedName("small")
        val small:String?,
        @SerializedName("medium")
        val medium:String?,
        @SerializedName("large")
        val large:String?
    ): Parcelable
    }