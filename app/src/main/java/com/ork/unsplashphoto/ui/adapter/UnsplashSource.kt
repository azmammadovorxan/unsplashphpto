package com.ork.unsplashphoto.ui.adapter


import android.net.Uri
import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.ork.unsplashphoto.data.api.RetrofitInstance
import com.ork.unsplashphoto.data.api.UnsplashApi
import com.ork.unsplashphoto.data.model.UnsplashPhoto
import retrofit2.HttpException
import java.io.IOException

class UnsplashSource(private val unsplashApi: UnsplashApi, val query: Int) :
    PagingSource<Int, UnsplashPhoto>() {


    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, UnsplashPhoto> {

        Log.e("Tag", "Paging Source")
        return try {

            val nextPageNumber = params.key ?: FIRST_PAGE_INDEX

            val response = unsplashApi.getDataApi(query)

            Log.e("Tag", "Paging Source response ${response.body()}")
            return LoadResult.Page(
                data = response.body() ?: emptyList(),
                prevKey = null,
                nextKey = nextPageNumber + 1
            )


        } catch (e: IOException) {
            Log.e("Tag", "Paging Source io $e")
            LoadResult.Error(e)

        } catch (e: HttpException) {
            Log.e("Tag", "Paging Source http $e")
            LoadResult.Error(e)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, UnsplashPhoto>): Int? {
        return state.anchorPosition
    }


    companion object {
        private const val FIRST_PAGE_INDEX = 1
    }
}