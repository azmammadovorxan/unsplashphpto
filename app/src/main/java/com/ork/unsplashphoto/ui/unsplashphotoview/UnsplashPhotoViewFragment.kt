package com.ork.unsplashphoto.ui.unsplashphotoview

import android.app.WallpaperManager
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.ork.unsplashphoto.R
import com.ork.unsplashphoto.databinding.FragmentUnsplashPhotoViewBinding
import com.squareup.picasso.Picasso
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class UnsplashPhotoViewFragment : Fragment() {

    lateinit var binding:FragmentUnsplashPhotoViewBinding
    private val args: UnsplashPhotoViewFragmentArgs by navArgs()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentUnsplashPhotoViewBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val unsplashPhoto = args.photo

        Picasso.get().load(unsplashPhoto.urls.regular).into(binding.imageUrlFull)
//        Log.e("description", "Photo to description")
        binding.description.text = unsplashPhoto.description

        binding.floatingActionButton.setOnClickListener {
            setWallPaper(view)

            Toast.makeText(this.context, "Wallpaper set!", Toast.LENGTH_SHORT).show()


        }

    }

    private fun setWallPaper(view: View){
        lifecycleScope.launch(Dispatchers.IO) {
            Log.e("WallPaper","Wall Paper Error")
            val bitmap = Picasso.get().load(args.photo.urls.regular).get()
            val wallpaperManager = WallpaperManager.getInstance(context)
            wallpaperManager.setBitmap(bitmap)
        }


    }

}