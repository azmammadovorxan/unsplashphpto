package com.ork.unsplashphoto.ui.unsplashphoto

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.ork.unsplashphoto.databinding.FragmentUnsplashPhotoBinding
import com.ork.unsplashphoto.ui.adapter.UnsplashAdapter
import com.ork.unsplashphoto.viewmodel.UnsplashViewModel
import kotlinx.coroutines.flow.collectLatest

class UnsplashPhotoFragment : Fragment() {
    val unsplashViewModel: UnsplashViewModel by viewModels()
    lateinit var unsplashAdapter: UnsplashAdapter
    lateinit var binding: FragmentUnsplashPhotoBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentUnsplashPhotoBinding.inflate(inflater,container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recyclerViewUnsplash.apply {
            layoutManager = LinearLayoutManager(this.context)
            val decoration =DividerItemDecoration(context,DividerItemDecoration.VERTICAL)
            addItemDecoration(decoration)
            unsplashAdapter = UnsplashAdapter()
            adapter = unsplashAdapter
        }


        lifecycleScope.launchWhenCreated {
            unsplashViewModel.getListData().collectLatest {
                Log.e("Tag", it.toString())
                unsplashAdapter.submitData(it)
            }
        }
    }



}