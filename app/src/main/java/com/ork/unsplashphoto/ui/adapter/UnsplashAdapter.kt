package com.ork.unsplashphoto.ui.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ork.unsplashphoto.R
import com.ork.unsplashphoto.data.model.UnsplashPhoto
import com.ork.unsplashphoto.ui.unsplashphoto.UnsplashPhotoFragmentDirections

class UnsplashAdapter : PagingDataAdapter<UnsplashPhoto, UnsplashAdapter.MyViewHolder>(DiffUtilCallBack()) {

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(getItem(position)!!)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val inflater = LayoutInflater.from(parent.context).inflate(R.layout.item_unsplash_recyclerview,parent,false)
        return MyViewHolder(inflater)
    }

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val userProfileImage:ImageView = view.findViewById(R.id.user_profile_image)
        private val textViewUsername:TextView = view.findViewById(R.id.text_view_user_name)
        private  val imageViewUrls:ImageView = view.findViewById(R.id.image_view_urls)
        private val textViewDescription:TextView = view.findViewById(R.id.text_view_description)

        fun bind(data:UnsplashPhoto){
            textViewUsername.text = data.user.username
            textViewDescription.text = data.description
            Glide.with(userProfileImage)
                .load(data.user.profileImage.small)
                .circleCrop()
                .into(userProfileImage)
            Glide.with(imageViewUrls)
                .load(data.urls.regular)
                .into(imageViewUrls)

            Log.e("Arg", data.user.toString())
            textViewUsername.setOnClickListener{
                val direction = UnsplashPhotoFragmentDirections.actionUnsplashPhotoFragmentToUnsplashUserFragment(data.user)
                it.findNavController().navigate(direction)
            }
            imageViewUrls.setOnClickListener {
               // data.urls.description = data.description
                val direction =UnsplashPhotoFragmentDirections.actionUnsplashPhotoFragmentToUnsplashPhotoViewFragment(data)
                it.findNavController().navigate(direction)
            }

        }
    }

    class DiffUtilCallBack: DiffUtil.ItemCallback<UnsplashPhoto>(){
        override fun areItemsTheSame(oldItem: UnsplashPhoto, newItem: UnsplashPhoto): Boolean {
            return oldItem.user.username == newItem.user.username
        }

        override fun areContentsTheSame(oldItem: UnsplashPhoto, newItem: UnsplashPhoto): Boolean {
            return  oldItem.user.username == newItem.user.username
                    && oldItem.urls.full== newItem.urls.full
        }


    }

}