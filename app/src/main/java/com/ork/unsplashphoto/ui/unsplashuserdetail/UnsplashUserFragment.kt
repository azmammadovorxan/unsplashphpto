package com.ork.unsplashphoto.ui.unsplashuserdetail

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.ork.unsplashphoto.R
import com.ork.unsplashphoto.databinding.FragmentUnsplashUserBinding
import com.squareup.picasso.Picasso

class UnsplashUserFragment : Fragment() {

    private lateinit var binding: FragmentUnsplashUserBinding

    private val args: UnsplashUserFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentUnsplashUserBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.e("Arg", args.user.toString())
        val unsplash = args.user

        Picasso.get().load(unsplash.profileImage.large).into(binding.userProfileImage)
        binding.apply {
            textViewName.text = getString(R.string.name, unsplash.name)
            textViewUserName.text = getString(R.string.username, unsplash.username)
            unsplash.apply {
                twitter_username?.let {
                    textViewTwitterUsername.text = getString(R.string.twitter_username, it)
                    textViewTwitterUsername.isVisible = true
                }

               instagram_username?.let {
                    textViewInstagramUsername.text = getString(R.string.instagram_username, it)
                    textViewInstagramUsername.isVisible = true
                }



                portfolio_url?.let {
                    textViewPortfolioUrl.text = getString(R.string.portfolio_url, it)
                    textViewPortfolioUrl.isVisible = true
                }


                bio?.let {
                    textViewBio.text = getString(R.string.bio, it)
                    textViewBio.isVisible = true
                }
            }


        }


    }

}