package com.ork.unsplashphoto.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.ork.unsplashphoto.data.api.RetrofitInstance
import com.ork.unsplashphoto.data.api.UnsplashApi
import com.ork.unsplashphoto.data.model.UnsplashPhoto
import com.ork.unsplashphoto.ui.adapter.UnsplashSource
import kotlinx.coroutines.flow.Flow

class UnsplashViewModel:ViewModel() {

    private var retrofit:UnsplashApi = RetrofitInstance.getRetroInstance().create(UnsplashApi::class.java)

    fun getListData(): Flow<PagingData<UnsplashPhoto>> {
        return Pager(config = PagingConfig(pageSize = 5),
        pagingSourceFactory = {UnsplashSource(retrofit, 1)}).flow.cachedIn(viewModelScope)
    }
}
